
This file summarizes the compilation and execution process of the different 
components developed withing WP2. It is assumed a Debian based OS.

For the rest of this manual we are assuming the following base directories. The first one `AMPERE_PATH` for the installed tools and the second one `AMPERE_SRC_PATH` for the source code. It's recommended to add them into the .bashrc file.

```sh
export AMPERE_PATH=/opt/ampere
export AMPERE_SRC_PATH=/home/${USER}/ampere
export PATH=${AMPERE_PATH}/llvm/bin:${AMPERE_PATH}/extrae/bin:${AMPERE_PATH}/papi/bin:${PATH}
export LD_LIBRARY_PATH=${AMPERE_PATH}/llvm/lib:${LD_LIBRARY_PATH}
```

## 1 The Synthetic Load Generator (SLG) and the Amalthea model
(amalthea project. This project contains examples used by following steps)

The first step is to download and compile the source generator. Instructions 
are available inside the “Amalthea_SLG” readme file. The Amalthea model of the 
example used in this deliverable is present inside the “Amalthea_model” folder. 
After running the code generator for the given model an OpenMP source code is 
generated.

## 2 Build LLVM and OpenMP
(llvm project)

Source code is available under the LLVM AMPERE repository.
LLVM can be built as explained in the [official documentation](https://llvm.org/docs/CMake.html).
However, the following configuration is recommended and tested:

Install ninja build system and clang (optional):
```sh
sudo apt-get install ninja-build clang
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
```

Clone, configure and install LLVM.

**New**: Due to the increasing size of the LLVM repository and the BSC gitlab configuration now it is not possible to clone the entire repository. To obtain the LLVM sources you must clone with depth 1, use the new clone command:
```sh
cd ${AMPERE_SRC_PATH}
git clone -b omp-taskgraph --depth 1 https://gitlab.bsc.es/ampere-sw/wp2/llvm.git
cd llvm
mkdir build
cd build
cmake -DLLVM_ENABLE_PROJECTS="clang;openmp" -DCMAKE_BUILD_TYPE=MinSizeRel -G "Ninja" -DCMAKE_INSTALL_PREFIX=${AMPERE_PATH}/llvm -DLIBOMP_TASKGRAPH=1 ../llvm
ninja install
```

## 3 Using *taskgraph* directive to enable static / dynamic TDG

In a generic OpenMP example, the first step is to insert the new taskgraph directive inside the parallel region:

```C
#pragma omp parallel
#pragma omp single
{
  #pragma omp taskgraph tdg_type(static)
  {
    ...
  }
}
```

When using the OpenMP code generated with the AMALTHEA SLG, if the model contains the proper annotations, the taskgraph directive is already inserted.

Then, code can be compiled trough the two new clang flags:

- -fopenmp-taskgraph : Mandatory to parse taskgraph directives.
- -static-tdg : Mandatory to generate static TDG files.

To compile a static tdg, the clang command is:
```C
../llvm/bin/clang -fopenmp -fopenmp-taskgraph -static-tdg -g ....
```
**Note**: If you get an error for undefined symbols "__kmpc_set_task_static_id" or "
__kmpc_taskgraph", it means you are linking against the original OpenMP library and not the installed one with LLVM. Please add the correct path to the installed "libomp.so" library, under the "lib" folder of the LLVM installation, with the -L flag (e.g -L${AMPERE_PATH}/llvm/lib).

The generated output files for static tdgs, apart from the binary, are:

- tdg.cpp : A file that contains the TDG used by the KMP OpenMP library, needed for execution.
- tdg.dot : A file containing a visual graph representation of the TDG. Can be opened with the [xdot](https://pypi.org/project/xdot/) python application.

You can find several examples of benchmarks with static taskgraph in the [LLVM_Taskgraph_Benchmarks]( https://gitlab.bsc.es/ampere-sw/wp2/general-information) zip file.

## 4 Run performance analysis combined with TDG information
(extrae project)

Before installing Extrae we have to install PAPI, which is the bakend tool that access the platform dependent hardware counters. This procedure will change slightly depending on the target computer, i.e. desktop PC, NVIDIA Xavier, or Xilinx UltraScale+.

```sh
cd ${AMPERE_SRC_PATH}
git clone https://bitbucket.org/icl/papi.git
cd papi/src
./configure --prefix=${AMPERE_PATH}/papi
make -j 8 
sudo make install
```

Extrae source files and installation instructions are provided inside the 
“Extrae” repo. Generate the desired installation folder structure and build 
the Extrae. Use following configuration:

```bash
cd ${AMPERE_SRC_PATH}
git clone https://gitlab.bsc.es/ampere-sw/wp2/extrae.git
cd extrae
autoreconf -vfi
./configure --prefix=${AMPERE_PATH}/extrae --without-mpi --without-dyninst --without-unwind --with-papi=${AMPERE_PATH}/papi --enable-openmp-intel
make -j4
sudo make install
```
If you get an error about libiberty or libbfd libraries then make sure you have the binutils package installed in your system. In some systems binutils does not include the libiberty library and it is required to install the libiberty package separatedly. 

Navigate into the base folder of the Amalthea generated code to run the 
performance analysis (“amalthea_code_performance”):
- 1. Set-up:
```bash
sudo sh -c 'echo 1 >/proc/sys/kernel/perf_event_paranoid'
```
- 2. Manually instrument task code with Extrae functions:
```bash
Extrae_init();
...
Extrae_fini();
```
- 3. Instrumented code compilation:
Make sure that the instrumented source code of your task is compiled with Extrae support:
```bash
clang -I${AMPERE_PATH}/extrae/include -L${AMPERE_PATH}/extrae/lib <instrumented-source-files> -lomptrace
```
- 4. Extrae set-up and run:
```bash
export OMP_NUM_THREADS=4
export EXTRAE_CONFIG_FILE=${AMPERE_PATH}/extrae/share/example/OMP/extrae.xml
export LD_LIBRARY_PATH=${AMPERE_PATH}/extrae/lib:${LD_LIBRARY_PATH}
export EXTRAE_SKIP_AUTO_LIBRARY_INITIALIZE=1
./synthetic
```
- 5. Run python script:
The following script reads a TDG and Extrae traces, and augments the TDG with instrumentation information. First, checkout the script from its repository and then execute the following command:
```bash
cd ${AMPERE_SRC_PATH}
git clone https://gitlab.bsc.es/ampere-sw/wp2/tdg-instrumentation-script.git
cd <generated-source-code-directory>
python ${AMPERE_SRC_PATH}/tdg-instrumentation-script/parsePrvAndTdg.py tdg.dot synthetic.prv synthetic.pcf
```

- 6. Output will be .json file with the needed data.

## 5 Enable data preallocation

**Note**: This is an experimental feature. It may crash or contain bugs in certain circunstances. If you find a bug please contact adrian.munera@bsc.es or sara.royuela@bsc.es with detailed info.

The data preallocation mechanism provides the following benefits:

- 1. All the data needed by the runtime is allocated statically, so no dynamic memory is required. This avoids the usage of "malloc" calls in runtime, which are frecuently difficult to measure and error prone.

- 2. The user code inside the taskgraph is not required to be executed in the first taskgraph execution. This means that it saves some time in the first TDG execution (the following executions of the TDG never require to execute the user code, even withouth the data preallocation). This is possible because the task data can be known at compile time when using the data preallocation.

- 3. The runtime is able to allocate less task structures than tasks, saving memory. The user can specify in the taskgraph pragma (with num_preallocs clause) the number of tasks structures to be created. Usually, all the tasks of the TDG are never being executed at the same time because of dependencies. This allows several tasks to reuse the same task structure (the same memory space). The minimum number of tasks structures needed to not harm the execution time is the TDG width, which is the maximum number of tasks executing at the same time in a TDG. The user can specify any number of tasks structures to be allocated (from 1 to TDG size). If the number of task structures is lower than the TDG width, then the runtime will delay the execution of some tasks, harming the performance.

Overall, the data preallocation mechanism does not have a significant impact in performance if we use the correct number of tasks structures, but allows to save a lot of memory and disables the usage of dynamic memory. This is specially good for safety critical systems where we want to avoid the dynamic memory usage as much as possible, while reducing the amount of memory needed by the runtime. Also in memory limited systems, for TDGs with a high number of tasks the vanilla OpenMP runtime will need to allocate one task structure per each task, meanwhile if we use this mechanism, we only need to allocate the TDG width. This saves a huge amount of memory.

To activate this mechanism just add the "-prealloc" flag while compiling a static TDG:

```bash
../llvm/bin/clang -fopenmp -fopenmp-taskgraph -static-tdg -prealloc -g ....
```

Also, the number of preallocated task structures can be modified with the "num_preallocs" clause. By the default the number of task structures preallocated is the TDG size:

```C
#pragma omp parallel
#pragma omp single
{
  #pragma omp taskgraph tdg_type(static) num_preallocs(10)
  {
    ...
  }
}
```

## 6 Enable static scheduling

TDG tasks can be binded to a certain thread to obtain a static task scheduling policy. We will use the SparseLU benchmark as an example, but the process remains the same for all C applications. The steps to use the static scheduling are:

- 1. Compile the applicaton with static TDG support:
```bash
clang  -fopenmp -fopenmp-taskgraph -static-tdg -g -O2 sparseLU.c -o sparse
```
- 2. Modify the tdg.cpp file: For each task you want to statically schedule replace the member ".static_thread" with the desired thread id. By default, a "-1" means that the task can be scheduled in any thread. For example here we set a task binded to thread 2.
```bash
 .... .npredecessors = 1, .successors_size = 0, .static_thread = 2, .pragma_id = -1 ...
```
- 3. Recompile using the SparseLU object file and the modified C file "tdg.cpp", you also need to use the clang++ compiler and the "-fopenmp" flag:
```bash
clang++  -fopenmp sparseLU.o tdg.cpp -O2 -o sparse
```
- 4. Run the binary with the environment variable "OMP_TASK_SCHEDULE" set to "static":
```bash
OMP_TASK_SCHEDULE=static ./sparse 1
```

